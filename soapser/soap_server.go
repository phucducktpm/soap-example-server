package soapser

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"

	"github.com/achiku/xml"
)

// ProcessBRequest struct
type ProcessBRequest struct {
	XMLName   xml.Name `xml:"http://example.com/ns ProcessBRequest"`
	RequestID string   `xml:"RequestId"`
}

// ProcessARequest struct
type ProcessARequest struct {
	XMLName   xml.Name `xml:"http://example.com/ns ProcessARequest"`
	RequestID string   `xml:"RequestId"`
	SBAC      SBAC     `xml:"SBAC"`
}

type SBAC struct {
	A string `xml:"a"`
}

// ProcessAResponse struct
type ProcessAResponse struct {
	*AbstractResponse
	XMLName xml.Name `xml:"http://example.com/ns ProcessAResponse"`
	ID      string   `xml:"Id,omitifempty"`
	Process string   `xml:"Process,omitifempty"`
}

// ProcessBResponse struct
type ProcessBResponse struct {
	*AbstractResponse
	XMLName xml.Name `xml:"http://example.com/ns ProcessBResponse"`
	ID      string   `xml:"Id,omitifempty"`
	Process string   `xml:"Process,omitifempty"`
	Amount  string   `xml:"Amount,omitifempty"`
}

func processA() ProcessAResponse {
	return ProcessAResponse{
		AbstractResponse: &AbstractResponse{
			Code:   "201",
			Detail: "succe1ss",
		},
		ID:      "1100",
		Process: "Proce1ssAResponse",
	}
}

func processB() ProcessBResponse {
	return ProcessBResponse{
		AbstractResponse: &AbstractResponse{
			Code:   "200",
			Detail: "success",
		},
		ID:      "100",
		Process: "ProcessBResponse",
		Amount:  "10000",
	}
}

func soapActionHandler(w http.ResponseWriter, r *http.Request) {
	soapAction := r.Header.Get("soapAction")
	fmt.Println("soapAction:", soapAction)
	var res interface{}
	switch soapAction {
	case "processA":
		res = processA()
	case "processB":
		res = processB()
	default:
		res = nil
	}
	fmt.Println(res)
	v := SOAPEnvelope{
		Body: SOAPBody{
			Content: res,
			XMLName: xml.Name{
				Space: "this is",
				Local: "local",
			},
			Fault: &SOAPFault{
				Code: "this is code",
			},
		},
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/xml")
	x, err := xml.MarshalIndent(v, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(x)
	return
}

// <?xml version=""1.0"" encoding=""utf-8""?>
// <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
//   <soap:Body>
//     <NumberToDollars xmlns=""http://www.dataaccess.com/webservicesserver/"">
//       <dNum>500</dNum>
//     </NumberToDollars>
//   </soap:Body>
// </soap:Envelope>

type (
	DataRequest struct {
		EnvelopeRequest EnvelopeRequest `xml:"soapenv:Envelope"`
	}
	BodyRequest struct {
		XMLName xml.Name `xml:"Body"`
		Submit  Submit   `xml:"submit"`
	}
	EnvelopeRequest struct {
		XMLName xml.Name
		Body    BodyRequest `xml:",omitempty"`
		Header  Header      `xml:",omitempty"`
	}
	Header struct {
	}
	Submit struct {
		XMLName xml.Name `xml:"submit"`
		Request Request  `xml:"request"`
	}
	Request struct {
		// XMLName xml.Name `xml:"request"`
		// CData   string   `xml:",cdata"`
		Msg Msg `xml:"MSG"`
	}
	Msg struct {
		MsgHeader MsgHeader `xml:"HEADER"`
		MsgData   MsgData   `xml:"DATA"`
	}
	MsgHeader struct {
		SIGN         string `xml:"SIGN"`
		MESSAGE_CODE string `xml:"MESSAGE_CODE"`
	}
	MsgData struct {
		PROVIDER_ID string `xml:"PROVIDER_ID"`
		TRAN_DATE   string `xml:"TRAN_DATE"`
		SIGN        string `xml:"FILE_TYPE"`
	}
)

// <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
//    <soapenv:Header/>
//    <soapenv:Body>
//       <submit>
//          <request>
// 			<MSG>
//                 <HEADER>
//                     <MESSAGE_CODE>1001</MESSAGE_CODE>
//                     <SIGN>atuz4TaevGwGF1bGuSqpei5IvarD5F8hwsdrFYX9af8gcEszlEeSUKNuYjobN3nv2skH57S9wwqX3r4fWHGiZaG/hMf8iEgVPAj05kvBi8XbB0fAdCNXjX/Nawc3lReewcsptt10OGxqsbfGsfUgPWD99yhnq6RRhL4FvsQZA70=</SIGN>
//                 </HEADER>
//                 <DATA>
//                     <PROVIDER_ID>262</PROVIDER_ID>
//                     <TRAN_DATE>20221123</TRAN_DATE>
//                     <FILE_TYPE>1</FILE_TYPE>
//                 </DATA>
//          	</MSG>
//          </request>
//       </submit>
//    </soapenv:Body>
// </soapenv:Envelope>

// <soap:Envelope
// 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
// 	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
// 	xmlns:xsd="http://www.w3.org/2001/XMLSchema">
// 	<soap:Body>
// 		<submit>
// 			<request>
// 				<![CDATA[<MSG><HEADER><MESSAGE_CODE>1001</MESSAGE_CODE><SIGN>U3tLFgpML25UqTcqgfhXhenqvj7qUBbUlct2erqSiv4o2orje3b9bcaMi7vF/gMiPu+CU/gDPo7UdY11tZ6T+t2i4wO1C6tpVtR+TtpcEHwhFgzoTdvPFe2wLCW1gZlxmNUA+23/G5RKD2ajM4rLBnSJXxGfTwraxm5gi501qRchsNh7+0ZyOgmylap2I9Io6UE3Cp+BRe/31rV85aCtD4lnqbp1bn5unhpV4eW8epv6kj+I/Ke9FVQ3fGxYb0PU00bAYSxZJ4GvcPXeG4D5chxAbnRmyFBFj93LkjFkEo9ijdrKo0v4Vzwh3r1+pZmAV09tc1JxplRKbCGO3EGvMQ==</SIGN></HEADER><DATA><PROVIDER_ID>7477A0B7-0ABE-4BF2-8612-BBC01093B9F4</PROVIDER_ID><TRAN_DATE>20230109</TRAN_DATE><FILE_TYPE>1</FILE_TYPE><FILE_NAME>VPB-VDTC_20221123</FILE_NAME><FILE_CONTENT>MDF8MDF8RVBBU1N8RlQyMTI1MTEyOTk1ODA4MHxFUEFTUzAwSjExMTEyfDIwMDAwMDAwfFZORHwwMDAwMDF8MTEzMjAwfDIwMjIxMTExfDExMzIwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE4MTA3NzMwMjl8RVBBU1MwNE5OMTIxMXwyMDAwMDAwMXxWTkR8MDAwMDAyfDExMzMwMHwyMDIyMTExMXwxMTMzMDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMjY2MDc1MDEwfEVQQVNTMDBBMDAwMHwyMjJ8Vk5EfDAwMDAwM3wxMTM0MDB8MjAyMjExMTF8MTEzNDAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTEyOTk3MDg2MHxFUEFTUzAwQTAwMDB8MTExMTF8Vk5EfDAwMDAwNHwxMTM1MDB8MjAyMjExMTF8MTEzNTAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTUzNzc0NzEzMXxFUEFTUzAwQTAwMDB8MTAwMHxWTkR8MDAwMDA1fDExMzUwMHwyMDIyMTExMXwxMTM1MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMDg0ODUwMzE5fEVQQVNTMDBBMDAwMDB8MTExMXxWTkR8MDAwMDA2fDExMzcwMHwyMDIyMTExMXwxMTM3MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMzUyODIyNDgxfEVQQVNTMDBBMDAwMHwxMTExMXxWTkR8MDAwMDA3fDExMzgwMHwyMDIyMTExMXwxMTM4MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMDg0ODYyNDYwfEVQQVNTMDBBMDAwMHwxMTExfFZORHwwMDAwMDh8MTE0MDAwfDIwMjIxMTExfDExNDAwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE2MTMwNjEzMDh8RVBBU1MwMEEwMDAwfDEyMzN8Vk5EfDAwMDAwOXwxMjIwMDB8MjAyMjExMTF8MTIyMDAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTE5MDc1NjA5MXxFUEFTUzAwQTAwMDAwfDEyMjJ8Vk5EfDAwMDAxMHwxMjIyMDB8MjAyMjExMTF8MTIyMjAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTE3MDQ4MDAxM3xFUEFTUzAwQTAwMDB8MTExMTF8Vk5EfDAwMDAxMXwxMjI0MDB8MjAyMjExMTF8MTIyNDAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTE3MDQ4MzI3MnxFUEFTUzAwQTAwMDAwfDExMTExfFZORHwwMDAwMTJ8MTIyNTAwfDIwMjIxMTExfDEyMjUwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE1Njk0MTEwOTR8RVBBU1MwMEEwMDAwfDExMTExMXxWTkR8MDAwMDEzfDEyMjkwMHwyMDIyMTExMXwxMjI5MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMjE1MzAwMjI3fEVQQVNTMDBBMDAwMHwxMTExfFZORHwwMDAwMTR8MTIzNTAwfDIwMjIxMTExfDEyMzUwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTExMDg5NDE5OTl8RVBBU1MwMEEwMDAwfDExMTExfFZORHwwMDAwMTV8MTM0MjAwfDIwMjIxMTExfDEzNDIwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE4MjkwMjIzODh8RVBBU1NWMDAxMDAzOTE2fDEyMzEyMXxWTkR8MDAwMDE2fDE0MDkwMHwyMDIyMTExMXwxNDA5MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMDk1MDUyMjAwfEVQQVNTMDBLMTI0Nzh8MTAwMDAwfFZORHwwMDAwMTd8MTYwNzAwfDIwMjIxMTExfDE2MDcwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTEyODIxMDU5MDB8RVBBU1MwMDAwR0hIfDEwMDAwMDAwMHxWTkR8MDAwMDE4fDE4MzcwMHwyMDIyMTExMXwxODM3MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHwNCg==</FILE_CONTENT></DATA></MSG>]]>
// 			</request>
// 		</submit>
// 	</soap:Body>
// </soap:Envelope>

type (
	EnvelopeResponse struct {
		XMLName      xml.Name     `xml:"soapenv:Envelope"`
		Body         BodyResponse `xml:",omitempty"`
		XLMNSSoapenv string       `xml:"xmlns:soapenv,attr,omitempty"`
		XLMNSXsienv  string       `xml:"xmlns:xsi,attr,omitempty"`
		XLMNXSdenv   string       `xml:"xmlns:xsd,attr,omitempty"`
	}
	BodyResponse struct {
		XMLName xml.Name   `xml:"soapenv:Body"`
		Submit  SubmitResp `xml:"submit"`
	}
	SubmitResp struct {
		XMLName xml.Name    `xml:"submit"`
		Request RequestResp `xml:"request"`
	}
	RequestResp struct {
		MsgResp MsgResp `xml:"MSG"`
	}
	MsgResp struct {
		MsgHeader MsgHeaderResp `xml:"HEADER"`
		MsgData   MsgDataResp   `xml:"DATA"`
	}
	MsgHeaderResp struct {
		SIGN         string `xml:"SIGN"`
		MESSAGE_CODE string `xml:"MESSAGE_CODE"`
	}
	MsgDataResp struct {
		PROVIDER_ID  string `xml:"PROVIDER_ID"`
		TRAN_DATE    string `xml:"TRAN_DATE"`
		FILE_TYPE    string `xml:"FILE_TYPE"`
		FILE_NAME    string `xml:"FILE_NAME"`
		TRAN_CONTENT string `xml:"TRAN_CONTENT"`
	}
)

// response
func epassSoap(w http.ResponseWriter, r *http.Request) {
	rawbody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	rawbody = bytes.Replace(rawbody, []byte("<![CDATA["), []byte(""), -1)
	rawbody = bytes.Replace(rawbody, []byte("]]>"), []byte(""), -1)

	// bytes.NewReader(r.Body)

	var reqXML = EnvelopeRequest{}
	erR := xml.Unmarshal(rawbody, &reqXML)
	fmt.Println(reqXML.Body.Submit.Request)
	data := reqXML.Body.Submit.Request.Msg.MsgData.TRAN_DATE

	fmt.Println(erR)
	fmt.Println(fmt.Sprintf("%#v", reqXML))

	// response
	// var res interface{}
	v := EnvelopeResponse{
		XMLName:      xml.Name{},
		XLMNSSoapenv: "http://schemas.xmlsoap.org/soap/envelope/",
		XLMNSXsienv:  "http://www.w3.org/2001/XMLSchema-instance",
		XLMNXSdenv:   "http://www.w3.org/2001/XMLSchema",
		Body: BodyResponse{
			Submit: SubmitResp{
				Request: RequestResp{
					MsgResp: MsgResp{
						MsgHeader: MsgHeaderResp{
							SIGN:         "U3tLFgpML25UqTcqgfhXhenqvj7qUBbUlct2erqSiv4o2orje3b9bcaMi7vF/gMiPu+CU/gDPo7UdY11tZ6T+t2i4wO1C6tpVtR+TtpcEHwhFgzoTdvPFe2wLCW1gZlxmNUA+23/G5RKD2ajM4rLBnSJXxGfTwraxm5gi501qRchsNh7+0ZyOgmylap2I9Io6UE3Cp+BRe/31rV85aCtD4lnqbp1bn5unhpV4eW8epv6kj+I/Ke9FVQ3fGxYb0PU00bAYSxZJ4GvcPXeG4D5chxAbnRmyFBFj93LkjFkEo9ijdrKo0v4Vzwh3r1+pZmAV09tc1JxplRKbCGO3EGvMQ==",
							MESSAGE_CODE: "112233",
						},
						MsgData: MsgDataResp{
							FILE_TYPE:    "1",
							FILE_NAME:    "HDBANK-VDTC_20221123",
							TRAN_CONTENT: "MDF8MDF8RVBBU1N8RlQyMTI1MTEyOTk1ODA4MHxFUEFTUzAwSjExMTEyfDIwMDAwMDAwfFZORHwwMDAwMDF8MTEzMjAwfDIwMjIxMTExfDExMzIwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE4MTA3NzMwMjl8RVBBU1MwNE5OMTIxMXwyMDAwMDAwMXxWTkR8MDAwMDAyfDExMzMwMHwyMDIyMTExMXwxMTMzMDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMjY2MDc1MDEwfEVQQVNTMDBBMDAwMHwyMjJ8Vk5EfDAwMDAwM3wxMTM0MDB8MjAyMjExMTF8MTEzNDAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTEyOTk3MDg2MHxFUEFTUzAwQTAwMDB8MTExMTF8Vk5EfDAwMDAwNHwxMTM1MDB8MjAyMjExMTF8MTEzNTAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTUzNzc0NzEzMXxFUEFTUzAwQTAwMDB8MTAwMHxWTkR8MDAwMDA1fDExMzUwMHwyMDIyMTExMXwxMTM1MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMDg0ODUwMzE5fEVQQVNTMDBBMDAwMDB8MTExMXxWTkR8MDAwMDA2fDExMzcwMHwyMDIyMTExMXwxMTM3MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMzUyODIyNDgxfEVQQVNTMDBBMDAwMHwxMTExMXxWTkR8MDAwMDA3fDExMzgwMHwyMDIyMTExMXwxMTM4MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMDg0ODYyNDYwfEVQQVNTMDBBMDAwMHwxMTExfFZORHwwMDAwMDh8MTE0MDAwfDIwMjIxMTExfDExNDAwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE2MTMwNjEzMDh8RVBBU1MwMEEwMDAwfDEyMzN8Vk5EfDAwMDAwOXwxMjIwMDB8MjAyMjExMTF8MTIyMDAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTE5MDc1NjA5MXxFUEFTUzAwQTAwMDAwfDEyMjJ8Vk5EfDAwMDAxMHwxMjIyMDB8MjAyMjExMTF8MTIyMjAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTE3MDQ4MDAxM3xFUEFTUzAwQTAwMDB8MTExMTF8Vk5EfDAwMDAxMXwxMjI0MDB8MjAyMjExMTF8MTIyNDAwfDIwMjIxMTExfHwyNjgyMjM3Nzh8fHx8fHx8fA0KMDF8MDF8RVBBU1N8RlQyMTI1MTE3MDQ4MzI3MnxFUEFTUzAwQTAwMDAwfDExMTExfFZORHwwMDAwMTJ8MTIyNTAwfDIwMjIxMTExfDEyMjUwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE1Njk0MTEwOTR8RVBBU1MwMEEwMDAwfDExMTExMXxWTkR8MDAwMDEzfDEyMjkwMHwyMDIyMTExMXwxMjI5MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMjE1MzAwMjI3fEVQQVNTMDBBMDAwMHwxMTExfFZORHwwMDAwMTR8MTIzNTAwfDIwMjIxMTExfDEyMzUwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTExMDg5NDE5OTl8RVBBU1MwMEEwMDAwfDExMTExfFZORHwwMDAwMTV8MTM0MjAwfDIwMjIxMTExfDEzNDIwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTE4MjkwMjIzODh8RVBBU1NWMDAxMDAzOTE2fDEyMzEyMXxWTkR8MDAwMDE2fDE0MDkwMHwyMDIyMTExMXwxNDA5MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHx8DQowMXwwMXxFUEFTU3xGVDIxMjUxMDk1MDUyMjAwfEVQQVNTMDBLMTI0Nzh8MTAwMDAwfFZORHwwMDAwMTd8MTYwNzAwfDIwMjIxMTExfDE2MDcwMHwyMDIyMTExMXx8MjY4MjIzNzc4fHx8fHx8fHwNCjAxfDAxfEVQQVNTfEZUMjEyNTEyODIxMDU5MDB8RVBBU1MwMDAwR0hIfDEwMDAwMDAwMHxWTkR8MDAwMDE4fDE4MzcwMHwyMDIyMTExMXwxODM3MDB8MjAyMjExMTF8fDI2ODIyMzc3OHx8fHx8fHwNCg==",
							TRAN_DATE:    data,
							PROVIDER_ID:  "7477A0B7-0ABE-4BF2-8612-BBC01093B9F4",
						},
					},
				},
			},
		},
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/xml")
	x, err := xml.MarshalIndent(v, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(x)
	return
}

func soapBodyHandler(w http.ResponseWriter, r *http.Request) {

	rawbody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// bytes.NewReader(r.Body)

	var reqXML = EnvelopeRequest{}
	erR := xml.Unmarshal(rawbody, &reqXML)

	fmt.Println(erR)
	fmt.Println(fmt.Sprintf("%#v", reqXML))

	// var reqXM1L = DataRequest{}
	// erRx := xml.Unmarshal(rawbody, &reqXM1L)
	// fmt.Println(erRx)
	// fmt.Println(fmt.Sprintf("%#v", reqXM1L))

	a := regexp.MustCompile(`<ProcessARequest xmlns="http://example.com/ns">`)
	b := regexp.MustCompile(`<ProcessBRequest xmlns="http://example.com/ns">`)

	fmt.Println(a, b)

	var res interface{}
	if a.MatchString(string(rawbody)) {
		res = processA()
	} else if b.MatchString(string(rawbody)) {
		res = processB()
	} else {
		res = nil
	}
	v := SOAPEnvelope{
		Body: SOAPBody{
			Content: res,
		},
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/xml")
	x, err := xml.MarshalIndent(v, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(x)
	return
}

// NewSOAPMux return SOAP server mux
func NewSOAPMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/dispatch/soapaction", soapActionHandler)
	mux.HandleFunc("/dispatch/soapbody", soapBodyHandler)
	mux.HandleFunc("/epass/compare", epassSoap)

	return mux
}

// NewSOAPServer create i2c mock server
func NewSOAPServer(port string) *http.Server {
	mux := NewSOAPMux()
	server := &http.Server{
		Handler: mux,
		Addr:    "localhost:" + port,
	}
	fmt.Println("start server at port:", port)
	server.ListenAndServe()
	return server
}
