package main

import (
	"context"
	"encoding/xml"
	"log"

	"github.com/globusdigital/soap"
)

// FooRequest a simple request
type FooRequest struct {
	XMLName xml.Name `xml:"fooRequest"`
	Foo     string
}

// FooResponse a simple response
type FooResponse struct {
	Bar string
}

func main() {
	client := soap.NewClient("http://127.0.0.1:1234/", nil)
	response := &FooResponse{}
	httpResponse, err := client.Call(context.Background(), "processA", &FooRequest{Foo: "hello i am foo"}, response)
	if err != nil {
		panic(err)
	}
	log.Println(response.Bar, httpResponse.Status)
}
