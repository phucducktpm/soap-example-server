package main

import (
	"os"
	"os/signal"
	"syscall"
	"test-soap/soapser"
)

// // FooRequest a simple request
// type FooRequest struct {
// 	XMLName xml.Name `xml:"fooRequest"`
// 	Foo     string
// }

// // FooResponse a simple response
// type FooResponse struct {
// 	Bar string
// }

// func main() {
// 	client := soap.NewClient("http://127.0.0.1:8080/", nil)
// 	response := &FooResponse{}
// 	httpResponse, err := client.Call(context.Background(), "operationFoo", &FooRequest{Foo: "hello i am foo"}, response)
// 	if err != nil {
// 		panic(err)
// 	}
// 	log.Println(response.Bar, httpResponse.Status)
// }

func main() {
	soapser.NewSOAPServer("8000")
	//
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	done := make(chan bool, 1)
	go func() {
		<-sigs
		done <- true
	}()
	<-done
}
